<?php

namespace Tests\Feature;

use App\User;
use Faker\Provider\Lorem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Laravel\Passport\Passport;

class PostTest extends TestCase
{
    use RefreshDatabase;

    protected function getPostData()
    {
        return [
            'title' => Lorem::sentence(),
            'description' => Lorem::text(500)
        ];
    }

    protected function createPost($data)
    {
        $response = $this->postJson(
            route('post.store'),
            $data
        );

        $response->assertStatus(201);

        return $response;
    }

    protected function updatePost($id)
    {
        $this->patchJson(
            route(
                'post.update',
                ['id' => $id]
            ),
            [
                'title' => 'Modified',
                'description' => 'Modified'
            ]
        )
            ->assertStatus(422);

        $this->patchJson(
            route(
                'post.update',
                ['id' => $id]
            ),
            [
                'title' => 'Modified',
                'description' => 'Modified to very long description'
            ]
        )
            ->assertStatus(200);
    }


    protected function showPost($id)
    {
        return $this->getJson(
            route(
                'post.show',
                ['id' => $id]
            )
        );
    }

    /**
     * Test index method.
     *
     * @return void
     */
    public function testIndex()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $post = $this->getPostData();

        $this->createPost($post);

        $response = $this->getJson(
            route('post.index')
        );

        $response->assertStatus(200);
        $this->assertEquals(1, count($response->json()));
        $this->assertEquals($post['title'], $response->json()[0]['title']);
        $this->assertEquals($post['description'], $response->json()[0]['description']);
    }

    /**
     * Test store method.
     *
     * @return void
     */
    public function testStore()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $this->createPost($this->getPostData());
    }

    /**
     * Test show method.
     *
     * @return void
     */
    public function testShow()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $post = $this->getPostData();

        $create = $this->createPost($post);

        $response = $this->showPost($create->json('id'));

        $response->assertStatus(200);
        $this->assertEquals($post['title'], $response->json('title'));
        $this->assertEquals($post['description'], $response->json('description'));
    }

    /**
     * Test update method.
     *
     * @return void
     */
    public function testUpdate()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $post = $this->getPostData();

        $create = $this->createPost($post);

        $this->updatePost($create->json('id'));

        $response = $this->showPost($create->json('id'));

        $response->assertStatus(200);
        $this->assertNotEquals($post['title'], $response->json('title'));
        $this->assertNotEquals($post['description'], $response->json('description'));
    }

    /**
     * Test destroy method.
     *
     * @return void
     */
    public function testDestroy()
    {
        Passport::actingAs(
            factory(User::class)->create()
        );

        $post = $this->getPostData();

        $create = $this->createPost($post);

        $this->deleteJson(
            route(
                'post.destroy',
                ['id' => $create->json()['id']]
            )
        );

        $response = $this->showPost($create->json('id'));

        $response->assertStatus(404);
    }
}
